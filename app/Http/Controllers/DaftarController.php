<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\Province;

class DaftarController extends Controller
{
    public function index() {
        $olimpiades = DB::table('olimpiades')
            ->where('is_active', 1)
            ->get();
        $provinces = Province::pluck('name', 'id');

        return view('index')->with(['provinces' => $provinces, 'olimpiades' => $olimpiades,]);
    }

    public function kota(Request $request) {
        $cities = City::where('province_id', $request->get('id'))->pluck('name', 'id');

        return response()->json($cities);
    }

    public function daftar(Request $request) {
        $request->validate([
            'nama_lengkap' => 'required',
            'username' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'no_telepon_whatsapp' => 'required',
            'provinsi_id' => 'required',
            'kabupaten_kota_id' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ], [
            'nama_lengkap.required' => 'Nama tidak boleh kosong',
            'username.required' => 'username tidak boleh kosong',
            'tanggal_lahir.required' => 'Tanggal Lahir tidak boleh kosong',
            'jenis_kelamin.required' => 'Jenis Kelamin tidak boleh kosong',
            'alamat.required' => 'Alamat tidak boleh kosong',
            'no_telepon_whatsapp.required' => 'No. Telp. tidak boleh kosong',
            'provinsi_id.required' => 'Provinsi tidak boleh kosong',
            'kabupaten_kota_id.required' => 'Kabupaten tidak boleh kosong',
            'asal_sekolah.required' => 'Asal Sekolah tidak boleh kosong',
            'kelas.required' => 'Kelas tidak boleh kosong',
            'email.required' => 'E-mail sudah terdaftar, gunakan e-mail lain.',
            'password.required' => 'Password harus diisi.'
        ]);
        $data = array();
        $data['created_at'] = new \DateTime();
        $user = array(
            'nama_lengkap'          => $request->nama_lengkap,
            'username'              => $request->username,
            'tanggal_lahir'         => $request->tanggal_lahir,
            'tempat_lahir'          => $request->tempat_lahir,
            'jenis_kelamin'         => $request->jenis_kelamin,
            'alamat'                => $request->alamat,
            'no_telepon_whatsapp'   => $request->no_telepon_whatsapp,
            'provinsi_id'           => $request->provinsi_id,
            'kabupaten_kota_id'     => $request->kabupaten_kota_id,
            'asal_sekolah'          => $request->asal_sekolah,
            'kelas'                 => $request->kelas,
            'email'                 => $request->email,
            'password'              => bcrypt($request->password),
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString()
        );

        $olimpiades = array(
            'olimpiade_id' => $request->olimpiade_id,
        );

        // dump($olimpiades);

        $save = DB::table('users')->insert($user);
        $last_id = DB::getPDO()->lastInsertId();

        foreach ($olimpiades as $olimpic) {
            foreach ($olimpic as $key => $value) {
                $attach = DB::table('olimpiade_user')->insert([
                    'olimpiade_id' => $value,
                    'user_id' => $last_id,
                ]);
            }
        }
        
        return view('berhasil');
    }
}

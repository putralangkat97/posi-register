<?php

use App\Tingkat;
use Illuminate\Database\Seeder;

class TingkatSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tingkat::truncate();

        Tingkat::create([
            'tingkat' => 'SMA/SMK'
        ]);

        Tingkat::create([
            'tingkat' => 'SMP/MTs'
        ]);
    }
}

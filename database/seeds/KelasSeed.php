<?php

use App\Kelas;
use Illuminate\Database\Seeder;

class KelasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kelas::truncate();

        Kelas::create([
            'kelas' => 7
        ]);

        Kelas::create([
            'kelas' => 8
        ]);

        Kelas::create([
            'kelas' => 9
        ]);

        Kelas::create([
            'kelas' => 10
        ]);

        Kelas::create([
            'kelas' => 11
        ]);

        Kelas::create([
            'kelas' => 12
        ]);
    }
}

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Register | InSight</title>
  <link rel="stylesheet" type="text/css" href="{{asset('css/select2.min.css')}}">
  <link rel="shortcut icon" type="image/png" href="{{asset('img/favicon.png')}}" >
  <link href="https://fonts.googleapis.com/css2?family=Titillium+Web&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/main.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/gijgo.min.css')}}">
</head>

<body>

<div class="uk-grid-collapse" data-uk-grid style="font-family: 'Titillium Web', sans-serif;">
  <div class="uk-width-1-2m uk-padding-large uk-flex uk-flex-middle uk-flex-center uk-light
  uk-background-cover uk-background-norepeat uk-background-blend-overlay"
  style="background-image: url(img/bg.jpeg);" data-uk-height-viewport>
  <!-- <div>
  <div class="uk-text-center">
  <h2 class="uk-h1 uk-letter-spacing-small">Welcome Back</h2>
  </div>
  <div class="uk-margin-top uk-margin-medium-bottom uk-text-center">
  <p>Already signed up, enter your details and start the learning today</p>
  </div>
  <div class="uk-width-1-1 uk-text-center">
  <a href="sign-in.html" class="uk-button uk-button-success-outline uk-button-large">Sign In</a>
  </div>
  </div> -->
  </div>
  <div class="uk-width-1-2@m uk-padding-large uk-flex uk-flex-middle uk-flex-center" data-uk-height-viewport>
    <div class="uk-width-3-4@s">
      <div class="uk-text-center uk-margin-bottom">
        <a class="uk-logo uk-text-success uk-text-bold" href="/"><img src="img/logo-web.png" style="max-width: 230px;margin-left: "></a>
      </div>
      <form method="POST" action="{{ route('daftar.register') }}">
      @csrf
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="nama_lengkap">Nama Lengkap<span class="text-danger"><strong>*</strong></span></label>
              <input type="text" name="nama_lengkap" class="form-control" id="nama_lengkap" placeholder="Masukkan Nama" required  style="background-color: #F8F8FF;">
              @if ($errors->has('nama_lengkap')) <span class="text-danger">{{ $errors->first('nama_lengkap') }}</span> @endif
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="jenis_kelamin">Jenis Kelamin<span class="text-danger"><strong>*</strong></span></label>
              <select id="jenis_kelamin" name="jenis_kelamin" required class="form-control" style="background-color: #F8F8FF;">
                <option value="">Pilih</option>
                <option value="Laki-Laki">Laki-laki</option>
                <option value="Perempuan">Perempuan</option>
              </select>
              @if ($errors->has('jenis_kelamin')) <span class="text-danger">{{ $errors->first('jenis_kelamin') }}</span> @endif
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="tanggal_lahir">Tanggal Lahir<span class="text-danger"><strong>*</strong></span></label>
              <input type="text" name ="tanggal_lahir" id="tanggal_lahir" class="datepicker form-control" autocomplete="off" placeholder="Piih Tanggal Lahir" required  style="background-color: #F8F8FF;" readonly>
              @if ($errors->has('tanggal_lahir')) <span class="text-danger">{{ $errors->first('tanggal_lahir') }}</span> @endif
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="tempat_lahir">Tempat Lahir<span class="text-danger"><strong>*</strong></span></label>
              <input type="text" name ="tempat_lahir" id="tempat_lahir" class="form-control" autocomplete="off" placeholder="Tempat Lahir" required  style="background-color: #F8F8FF;">
              @if ($errors->has('tempat_lahir')) <span class="text-danger">{{ $errors->first('tempat_lahir') }}</span> @endif
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="no_telepon_whatsapp">No. Telp/WhatsApp/Telegram<span class="text-danger"><strong>*</strong></span></label>
              <input type="text" name ="no_telepon_whatsapp" id="no_telepon_whatsapp" class="form-control" autocomplete="off" placeholder="08xxxxx" required minlength="10" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" maxlength="15" style="background-color: #F8F8FF;">
              @if ($errors->has('no_telepon_whatsapp')) <span class="text-danger">{{ $errors->first('no_telepon_whatsapp') }}</span> @endif
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="alamat">Alamat</label>
              <textarea class="form-control" id="alamat" rows="2" name="alamat" required placeholder="Alamat" style="background-color: #F8F8FF;"></textarea>
              @if ($errors->has('alamat')) <span class="text-danger">{{ $errors->first('alamat') }}</span> @endif
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="asal_sekolah">Nama Sekolah<span class="text-danger"><strong>*</strong></span></label>
              <input type="text" name="asal_sekolah" id="asal_sekolah" class="form-control" placeholder="Nama Sekolah" required style="background-color: #F8F8FF;">
              @if ($errors->has('asal_sekolah')) <span class="text-danger">{{ $errors->first('asal_sekolah') }}</span> @endif
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="kelas">Kelas<span class="text-danger"><strong>*</strong></span></label>
              <select name="kelas" id="kelas" class="form-control" required style="background-color: #F8F8FF;">
                <option value="">Pilih</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
              @if ($errors->has('kelas')) <span class="text-danger">{{ $errors->first('kelas') }}</span> @endif
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="provinsi_id">Provinsi<span class="text-danger"><strong>*</strong></span></label>
              <select id="provinsi_id" name="provinsi_id" class="form-control" style="background-color: #F8F8FF;">
                <option value="">Pilih</option>
                @foreach($provinces as $id => $name)
                  <option value="{{ $id }}">{{ $name }}</option>
                @endforeach
              </select>
              @if ($errors->has('provinsi_id')) <span class="text-danger">{{ $errors->first('provinsi_id') }}</span> @endif
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="kabupaten_kota_id">Kota/Kabupaten<span class="text-danger"><strong>*</strong></span></label>
              <select name="kabupaten_kota_id" id="kabupaten_kota_id" class="form-control"  style="background-color: #F8F8FF;">
                <option value="">Pilih</option>
              </select>
              @if ($errors->has('kabupaten_kota_id')) <span class="text-danger">{{ $errors->first('kabupaten_kota_id') }}</span> @endif
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="olimpiade_id">Pilih Event:<span class="text-danger"><strong>*</strong></span></label>
              <select id="olimpiade_id" name="olimpiade_id[]" multiple="multiple" class="js-example-basic-multiple form-control" style="background-color: #F8F8FF;" required>
                @foreach($olimpiades as $key => $olimpiade)
                  <option value="{{ $olimpiade->id }}">{{ $olimpiade->nama_olimpiade }} {{ $olimpiade->tingkat }}</option>
                @endforeach
              </select>
              @if ($errors->has('olimpiade_id')) <span class="text-danger">{{ $errors->first('olimpiade_id') }}</span> @endif
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="username">Username<span class="text-danger"><strong>*</strong></span></label>
              <input type="text" name="username" id="username" class="form-control" placeholder="Username" required style="background-color: #F8F8FF;">
              @if ($errors->has('username')) <span class="text-danger">{{ $errors->first('username') }}</span> @endif
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="email">E-Mail<span class="text-danger"><strong>*</strong></span></label>
              <input type="email" name="email" id="email" class="form-control" placeholder="Alamat E-mail" required style="background-color: #F8F8FF;">
              @if ($errors->has('email')) <span class="text-danger">{{ $errors->first('email') }}</span> @endif
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="password">Password<span class="text-danger"><strong>*</strong></span></label>
              <input type="password" name="password" id="password" class="form-control" placeholder="Password" required  style="background-color: #F8F8FF;">
              @if ($errors->has('password')) <span class="text-danger">{{ $errors->first('password') }}</span> @endif
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="password-confirm">{{ __('Confirm Password') }}</label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="off" placeholder="Confirm Password" style="background-color: #F8F8FF;" onchange="check();">
              <span id="tes"></span>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <button class="btn btn-primary btn-lg submit-btn btn-block" style="border-radius: 15px;">Daftar</button>
            </div>
          </div>
        </div>
      </form>
      <div class="wrapper mt-5 text-gray">
        <p class="footer-text">© 2020 | InSight MC/Xtend Indonesia. <br>Design by <a href="https://n56ht.com" target="_blank">InSight MarComm</a></p>
      </div>
    </div>
  </div>
</div>

<!-- Script -->
  <script src="{{ asset('js/jquery.js') }}"></script>
  <script src="{{asset('js/uikit.js')}}"></script>
  <script>
    $(document).ready(function() {
      $('.js-example-basic-multiple').select2();
      $('.datepicker').datepicker({
        uiLibrary: 'bootstrap',
        format: 'yyyy-mm-dd'
      });
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
      });
      $('#provinsi_id').on('change', function () {
        $.post('{{ route('dropdown.kota') }}', { id: $(this).val() }).then(function (response) {
           // console.log(response);
          $('#kabupaten_kota_id').empty();

          $.each(response, function (id, name) {
            $('#kabupaten_kota_id').append(new Option(name, id))
          });
        });
      });
    });
  </script>

  <script src="{{asset('js/jquery.js')}}"></script>
  <script src="{{asset('js/app.js')}}"></script>
  <script src="{{asset('js/select2.min.js')}}"></script>
  <script src="{{asset('js/off-canvas.js')}}"></script>
  <script src="{{asset('js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('js/misc.js')}}"></script>
  <script src="{{asset('js/settings.js')}}"></script>
  <script src="{{asset('js/todolist.js')}}"></script>
  <script src="{{asset('js/gijgo.min.js')}}"></script>
  <!-- endinject -->

  <script src="{{ asset('js/script.js') }}"></script>
</body>
</html>

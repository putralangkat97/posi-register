<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DaftarController@index');
Route::post('/', 'DaftarController@kota')->name('dropdown.kota');
Route::post('/register', 'DaftarController@daftar')->name('daftar.register');

Route::get('/testing', function(){
  return view('berhasil');
});

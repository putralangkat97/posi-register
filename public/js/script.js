function check() {
    var pass = $('#password').val();
    var conpass = $('#password-confirm').val();

    if (pass != conpass) {
        $('#tes').html('Password tidak cocok!').css('color', 'red');
    } else {
        $('#tes').html('Password cocok').css('color', 'green');
    }
}

$(document).ready(function () {
    $('#password-confirm').keyup(check);

    $("input#username").on({
        keydown: function(e) {
            if (e.which === 32)
            return false;
        },
        change: function() {
            this.value = this.value.replace(/\s/g, "");
        }
    });
})